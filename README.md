# sams-hub-creator

Modul zum einfachen erstellen eines samsHUB basierend auf einer vorgegebenen
Verzeichnisstruktur unf Konfigurationsdateien im YAML-Format.

## Verwendung

Man kann das modul als python package z.B. in folgender Struktur Verwenden:
```
projekt
+-- config
|   +-- config.yaml
+-- sams_hub_creator
|   +-- __init__.py
|   ...
+-- app_package
|   +-- __init__.py
|   ...
+-- app.py
```

Dabei liegt diesen Projekt im Verzeichnis `sams_hub_creator`.
Ein amsHub Objektlässt sich in der app.py dann sehr einfach Erzeugen:
```python
import sams_hub_creator

hub = sams_hub_creator.create(conf_part_hub = 'my_hub')
```
In diesem Fall muss in der config.yaml der key für die hub konfiguration 'my_hub'
lauten. Die namen der apps, die der hub einbinden soll müssen in der config unter
'apps' stehen. Eine Config für die oben genannte Struktur könnte also so aussehen:
```yaml
zib_hub:
  default_language: de
  main_app: 'app_package'
apps:
  - name: 'app_package'
```

## Verweise
Zu den möglichkeiten der samsHub Konfiguration siehe 
[sams-classes](https://git.imp.fu-berlin.de/coding-at-fu/sams-classes)